# build env
FROM node:16-alpine AS builder

ENV PORT=4200

WORKDIR /app

COPY package.json ./

RUN npm install

COPY . ./

RUN npm run build


# production env
FROM nginx:stable-alpine

COPY --from=builder /app/build /usr/share/nginx/html

COPY nginx.conf /etc/nginx/conf.d/default.conf

CMD sed -i -e 's/$PORT/'"$PORT"'/g' /etc/nginx/conf.d/default.conf && nginx -g 'daemon off;'
