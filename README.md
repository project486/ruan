# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

### `npm start`

Runs the app in the development mode.\
Open http://127.0.0.1 to view it in the browser.

## Dockerfile

I used alpine to keep dockerfile the smallest size as much as possible and stable version for node.
Since it is small, it doesnt include many packages and programmes.
So I copied package.json file and `npm install` and then, can build `npm run build`
I got "permission denied" error while deployin Nginx to Heroku and solved the problem by following instructions in the document;
https://dev.to/levelupkoodarit/deploying-containerized-nginx-to-heroku-how-hard-can-it-be-3g14


## Gitlab CI/CD
I used Gitlab's shared runner, I defined my CI variables in the CI/CD sections on Gitlab, push docker image to DockerHub repository. 
My stages as follows;
- app-build
- app-test
- build
- deploy

Please see the gitlab-ci.yml file for more details.

## Deploy to Heroku
I deployed the react-app via Heroku cli. Defined required CD variables , Heroku api key in the CI/CD sections.
